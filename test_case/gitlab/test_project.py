# -*- coding: utf-8 -*-



from core.api.gitlab.project import Project
from core.api.gitlab.namespace import Namespace
from core.support.fake_data import FakeData

class TestProject(object):
    def test_get_all_projects(self):
        resp = Project().get_user_projects()
        assert resp.status_code == 200


    def test_project_workflow(self):
        # get user's namespace
        resp = Namespace().get_namespaces_info()
        assert resp.status_code == 200
        for namespace in resp.json():
            if namespace["name"] == "Tao Heng":
                namespace_id = namespace["id"]
                break
        # create project
        project_name = FakeData().fake_name()
        resp = Project().create_namespace_project(project_name, namespace_id)
        assert resp.status_code == 201
        print('Create project successfully!')
        assert resp.json()["name"] == project_name
        # delete project
        project_id = resp.json()["id"]
        resp = Project().delete_project(project_id)
        assert resp.status_code == 202
        print('Delete project successfully!')