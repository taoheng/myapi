# -*- coding: utf-8 -*-

import pytest
from core.api.reqres.users import Users


def test_users_api():
    resp = Users().get_single_user(1)
    assert resp.status_code == 200
    assert resp.json()["data"]["id"] == 1