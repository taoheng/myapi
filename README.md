##### 简介
实现了最基础的 ```pytest api``` 测试框架测试对象为三个:
1. "https://gitlab.com"
2. "http://reqres.in"
3.  "https://restful-booker.herokuapp.com/"

因为网站不同，所以对应的测试用例也不同，但是也对应了使用方式

##### 文件描述
```
--- core 核心库
     ｜
     ｜
     ｜- api_requests api 请求函数
     ｜
     ｜- gitlab gitlab api 对象封装
     ｜- login  login api 对象封装
     ｜- reqres reqres 对象封装
     ｜
     ｜- support api 依赖方法
     | - config 配置文件 
-----test_case 测试用例
-----conftest.py pytest 配置文件
----- requirements.txt 依赖库
```

##### 配置运行环境
运行环境为 `python3.8+`, 在文件目录下运行
```
pip install -r requirements.txt
```
即可安装依赖库
##### 运行测试
使用如下方法可以运行测试,在文件目录下
```
# 运行所有测试
pytest

# 运行指定目录测试
pytest test_case/gitlab #运行 gitlab 测试

# 传递测试 url
base-url=xxxx pytest test_case/gitlab
pytest --base-url xxxx

# 传递测试环境
env=xxxx pytest
pytest --env xxxx

# 指定测试对象
users=xxxx pytest
pytest --users xxxx
```
需要注意的是，`users` 只能在 `ADMIN`, `AGENT`, `SUBAGENT` 中做选择，而 `env` 只能选择 `SIT`, `UAT`, `STAGING`

