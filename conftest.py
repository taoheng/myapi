# -*- coding: utf-8 -*-
import os
import pytest

from core.api.api_requests import ApiRequests
from core.api.assertion import Assert
from core.support.env import get_yaml_config

env = "SIT"
# set global variables
# url: the url we test against
# headers: the headers we use
def pytest_configure(config):
    env_vars = get_yaml_config()

    # get user name from yaml
    pytest.admin_user = env_vars["USER"]["ADMIN"]
    pytest.agent_user = env_vars["USER"]["AGENT"]
    pytest.sub_agent_user = env_vars["USER"]["SUBAGENT"]
    pytest.admin_user_id = env_vars["USER"]["ADMINID"]

    # get sensitive info from env
    pytest.admin_password = os.getenv("ADMIN_PASSWORD", None)
    pytest.agent_password = os.getenv("AGENT_PASSWORD", None)
    pytest.sub_agent_password = os.getenv("SUBAGENT_PASSWORD", None)
    pytest.admin_token = os.getenv("ADMIN_TOKEN", None)
    pytest.agent_token = os.getenv("AGENT_TOKEN", None)
    pytest.sub_agent_token = os.getenv("SUBAGENT_TOKEN", None)
    
    # set app url
    base_url = config.getoption("base_url")
    if base_url:
        pytest.url = base_url
    else:
        pytest.url = env_vars["ENV"][env]
    # set users
    users = config.getoption("users")
    if users == "ADMIN":
        pytest.user = pytest.admin_user
        pytest.password = pytest.admin_password
        pytest.token = pytest.admin_token
    elif users == "AGENT":
        pytest.user = pytest.agent_user
        pytest.password = pytest.agent_password
        pytest.token = pytest.agent_token
    elif users == "SUBAGENT":
        pytest.user = pytest.sub_agent_user
        pytest.password = pytest.sub_agent_password
        pytest.token = pytest.sub_agent_token
    
# base url fixture
@pytest.fixture(scope="session")
def base_url(request):
    """ Base url """
    base_url = request.config.getoption("base_url")
    if not base_url:
        return base_url
    
def pytest_addoption(parser):
    parser.addoption("--base_url", 
                     action="store", 
                     default=os.getenv("PYTEST_BASE_URL", None), 
                     help="base url for api test")
    

# base env fixture

@pytest.fixture(scope="session")
def test_env(request):
    """ Test env """
    env = request.config.getoption("env")
    if not env:
        return env
    
def pytest_addoption(parser):
    parser.addoption("--env", 
                     action="store", 
                     default=os.getenv("PYTEST_ENV", None), 
                     help="test env for api test")
    
# set users fixture
@pytest.fixture(scope="session")
def users(request):
    """ Set users """
    users = request.config.getoption("users")
    if not users:
        return users


def pytest_addoption(parser):
    parser.addoption("--users",
                     action="store",
                     default=os.getenv("PYTEST_USERS", None),
                     help="users for api test")