# -*- coding: utf-8 -*-

import yaml
import pytest

from pathlib import Path

def get_yaml_config():
    """ Get yaml config """
    paths = ["core", "config", "config.yml"]
    fp = Path.cwd().joinpath(*paths)
    with open(fp, "r") as f:
        return yaml.safe_load(f)
    

    
