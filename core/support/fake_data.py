# -*- coding: utf-8 -*-

from faker import Faker

class FakeData(object):
    def __init__(self) -> None:
        pass 

    def fake_name(self) -> str:
        """ Fake name """
        return f"test_project_{Faker().random_int(0, 100000)}"