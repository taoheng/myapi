# -*- coding:utf-8 -*-

import pytest
from core.api.api_requests import ApiRequests

class AdminLogin(object):
    def __init__(self) -> None:
        pass

    def normal_login(self,
                     username=pytest.admin_user,
                     password=pytest.admin_password) -> object:
        """ Normal login """
        data = {
            "username": username,
            "password": password
        }

        headers = {
            "Content-Type": "application/json"
        }

        url = f"{pytest.url}/auth"
        return ApiRequests().post(url, data=data, headers=headers)