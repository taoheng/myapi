# -*- coding:utf-8 -*-
import pytest

from core.api.api_requests import ApiRequests


class Project(object):
    def __init__(self, token=pytest.admin_token,
                          id=pytest.admin_user_id):
        self.token = token
        self.user_id = id
        self.headers = {
            "PRIVATE-TOKEN": self.token,
        }

    def get_user_projects(self):
        # use the admin id
        url = f"{pytest.url}/api/v4/users/{self.user_id}/projects"
        return ApiRequests().get(url, headers=self.headers)


    def create_namespace_project(self, project_name, namespace_id):
        data = {   
            "name": project_name,
            "path": project_name,
            "namespace_id": namespace_id,
            "description": "test project",
            "initialize_with_readme": "true"
        }
        url = f"{pytest.url}/api/v4/projects"

        return ApiRequests().post(url, data=data, headers=self.headers)


    def delete_project(self, project_id):
        url = f"{pytest.url}/api/v4/projects/{project_id}"
        return ApiRequests().delete(url, headers=self.headers)