# -*- coding:utf-8 -*-

import pytest 
from core.api.api_requests import ApiRequests

class Namespace(object):
    def __init__(self, token=pytest.admin_token):
        self.headers = {
            "PRIVATE-TOKEN": token,
            "Content-Type": "application/json"
        }
        self.url = f"{pytest.url}/api/v4/namespaces"
    
    def get_namespaces_info(self):
        return ApiRequests().get(self.url, headers=self.headers)