# -*- coding: utf-8 -*-

import requests
import urllib3

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)


class ApiRequests(object):
    """ Api requests class """

    def __init__(self) -> None:
        pass

    def get(self, url: str, headers: dict = None, params: dict = None) -> requests.Response:
        """ Get request """
        print(f"Get {url}: ", url)
        return requests.get(url, headers=headers, params=params, verify=False)
    
    def post(self, url: str, headers: dict = None, data: dict = None) -> requests.Response:
        print(f"Post {url}: ", url)
        """ Post request """
        return requests.post(url, headers=headers, data=data, verify=False)
    
    def put(self, url: str, headers: dict = None, data: dict = None) -> requests.Response:
        print(f"Put {url}: ", url)
        """ Put request """
        return requests.put(url, headers=headers, data=data, verify=False)
    
    def delete(self, url: str, headers: dict = None, data: dict = None) -> requests.Response:
        print(f"Delete {url}: ", url)
        """ Delete request """
        return requests.delete(url, headers=headers, data=data, verify=False)
    

