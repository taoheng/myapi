# -*- coding:utf-8 -*-

import pytest 

from core.api.api_requests import ApiRequests

class Users(object):
    def __init__(self) -> None:
        self.url = f"{pytest.url}/api/users"

    def get_users_list(self, params: dict=None ) -> object:
        """ Get users list """
        return ApiRequests().get(self.url, params=params)
    
    def get_single_user(self, user_id: int) -> object:
        """ Get single user """
        return ApiRequests().get(f"{self.url}/{user_id}")
    
    def create_user(self, data: dict) -> object:
        """ Create user """
        return ApiRequests().post(self.url, data=data)
    
    def update_user(self, user_id: int, data: dict) -> object:
        """ Update user """
        return ApiRequests().put(f"{self.url}/{user_id}", data=data)
    
    def delete_user(self, user_id: int) -> object:
        """ Delete user """
        return ApiRequests().delete(f"{self.url}/{user_id}")