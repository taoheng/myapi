# -*- coding: utf-8 -*-

class Assert(object):

    @classmethod
    def expect(cls, actual, expected) -> None:
        """ Assert that actual is equal to expected """
        assert actual == expected, f"Expected {expected}, but got {actual}"